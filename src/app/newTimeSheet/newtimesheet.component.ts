import { Component, OnInit } from '@angular/core';
import { EmployeeService } from '../services/employee.service';
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'newtimesheet',
  templateUrl: './newtimesheet.component.html',
  styleUrls: ['./newtimesheet.component.scss']
})
export class NewTimesheetComponent implements OnInit {
  sheets: any;
  optionSelected: any;
  optionSelectedTask: any;
  employees: any;
  tasks: any;
  sheet: any = {};
  currDate=new Date().toISOString().substring(0,10);
  constructor(private employeeService: EmployeeService, public activeRoute: ActivatedRoute, public _router: Router) {

  }

  ngOnInit() {
  let id = this.activeRoute.snapshot.paramMap.get("id")

    this.sheet.EmployeeId = Number(id);
    this.sheet.TaskId = 1;
    this.employeeService.getallemployees().subscribe(data => {
      this.employees = data;
    });

    this.employeeService.getalltasks().subscribe(data => {
      this.tasks = data;
    });


  }

  getEmpSheet(id) {
    this.employeeService.getEmployeeSheets(id).subscribe(data => {
      this.sheets = data;
      console.log(this.sheets);
    });
  }

  onOptionsSelected(event) {
    this.getEmpSheet(event);
  }

  saveSheet() {
    this.employeeService.saveNewSheet(this.sheet).subscribe(data => {
      this._router.navigate(['/timesheet', this.sheet.EmployeeId])
    });

  }

}
