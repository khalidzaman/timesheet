import { NgModule,APP_INITIALIZER } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TimesheetComponent } from './timesheet/timesheet.component'
import { NewTimesheetComponent } from './newtimesheet/newtimesheet.component'

import { EmployeeListComponent } from './employee/employee.component'



const appRoutes: Routes = [
    {path:'',redirectTo:'/employeelist',pathMatch:'full'},
    { path: 'employeelist', component: EmployeeListComponent },
    { path: 'timesheet/:id', component: TimesheetComponent },
    { path: 'newtimesheet/:id', component: NewTimesheetComponent },
];

@NgModule({
    imports: [
        RouterModule.forRoot(appRoutes)
    ],
    exports: [
        RouterModule
    ],
    providers:[]
})
export class AppRoutingModule { }